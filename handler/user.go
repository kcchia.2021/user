package handler

import (
	"context"
	"fmt"
	"github/epith/302-backend-user/db"
	"github/epith/302-backend-user/models"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/google/uuid"
)

type userContextKey string

var userIDKey = userContextKey("Id")

func users(router chi.Router) {
	router.Get("/", GetAllUsers)

	router.Route("/{Id}", func(router chi.Router) {
		router.Use(UserContext)
		router.Get("/", getUser)
		router.Put("/", updateUser)
		router.Delete("/", deleteUser)
	})

	router.Route("/signup", func(router chi.Router) {
		router.Post("/", createUser)
	})

	router.Route("/signin", func(router chi.Router) {
		router.Post("/", loginUser)
	})
}

func health(router chi.Router) {
	router.Get("/", GetHealth)
}

func UserContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		userId := chi.URLParam(r, "Id")
		if userId == "" {
			if err := render.Render(w, r, ErrorRenderer(fmt.Errorf("user ID is required"))); err != nil {
				log.Println(err)
			}
			return
		}
		ctx := context.WithValue(r.Context(), userIDKey, userId)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func GetHealth(w http.ResponseWriter, r *http.Request) {
	render.Status(r, http.StatusOK)
	render.JSON(w, r, map[string]string{"message": "Service is healthy.", "service": "user"})
}

func GetAllUsers(w http.ResponseWriter, r *http.Request) {
	users, err := dbInstance.GetAllUsers()
	if err != nil {
		if err := render.Render(w, r, ServerErrorRenderer(err)); err != nil {
			log.Println(err)
		}
		return
	}
	render.Status(r, http.StatusOK)
	render.JSON(w, r, users)
}

func createUser(w http.ResponseWriter, r *http.Request) {
	user := &models.UserRequest{}
	if err := render.Bind(r, user); err != nil {
		if err := render.Render(w, r, ErrBadRequest); err != nil {
			log.Println(err)
		}
		return
	}
	if err := dbInstance.AddUser(user.User); err != nil {
		if err := render.Render(w, r, ErrorRenderer(err)); err != nil {
			log.Println(err)
		}
		return
	}
	render.Status(r, http.StatusCreated)
	render.JSON(w, r, map[string]string{"user_id": user.UserID.String(), "username": user.Username})
}

func loginUser(w http.ResponseWriter, r *http.Request) {
	user := &models.UserRequest{}
	if err := render.Bind(r, user); err != nil {
		if err := render.Render(w, r, ErrBadRequest); err != nil {
			log.Println(err)
		}
		return
	}
	if err := dbInstance.LoginUser(user.User); err != nil {
		if err := render.Render(w, r, LoginErrorRenderer(err)); err != nil {
			log.Println(err)
		}
		return
	}
	render.Status(r, http.StatusOK)
	render.JSON(w, r, map[string]string{"user_id": user.UserID.String(), "username": user.Username})
}

func getUser(w http.ResponseWriter, r *http.Request) {
	userId, uuidErr := uuid.Parse(r.Context().Value(userIDKey).(string))
	if uuidErr != nil {
		if err := render.Render(w, r, ErrorRenderer(uuidErr)); err != nil {
			log.Println(err)
		}
		return
	}

	user, err := dbInstance.GetUserById(userId)
	if err != nil {
		if err == db.ErrNoMatch {
			if err := render.Render(w, r, ErrNotFound); err != nil {
				log.Println(err)
			}
		} else {
			if err := render.Render(w, r, ErrorRenderer(err)); err != nil {
				log.Println(err)
			}
		}
		return
	}
	render.Status(r, http.StatusOK)
	render.JSON(w, r, user)
}

func deleteUser(w http.ResponseWriter, r *http.Request) {
	userId, uuidErr := uuid.Parse(r.Context().Value(userIDKey).(string))
	if uuidErr != nil {
		if err := render.Render(w, r, ErrorRenderer(uuidErr)); err != nil {
			log.Println(err)
		}
		return
	}

	err := dbInstance.DeleteUser(userId)
	if err != nil {
		if err == db.ErrNoMatch {
			if err := render.Render(w, r, ErrNotFound); err != nil {
				log.Println(err)
			}
		} else {
			if err := render.Render(w, r, ServerErrorRenderer(err)); err != nil {
				log.Println(err)
			}
		}
		return
	}
	render.DefaultResponder(w, r, "Successfully Deleted")
}

func updateUser(w http.ResponseWriter, r *http.Request) {
	userId, uuidErr := uuid.Parse(r.Context().Value(userIDKey).(string))
	if uuidErr != nil {
		if err := render.Render(w, r, ErrorRenderer(uuidErr)); err != nil {
			log.Println(err)
		}
		return
	}
	user := &models.UserUpdateRequest{}

	if err := render.Bind(r, user); err != nil {
		if err := render.Render(w, r, ErrBadRequest); err != nil {
			log.Println(err)
		}
		return
	}
	retUser, err := dbInstance.UpdateUser(userId, user.User)
	if err != nil {
		if err == db.ErrNoMatch {
			if err := render.Render(w, r, ErrNotFound); err != nil {
				log.Println(err)
			}
		} else {
			if err := render.Render(w, r, ServerErrorRenderer(err)); err != nil {
				log.Println(err)
			}
		}
		return
	}
	render.Status(r, http.StatusOK)
	render.JSON(w, r, map[string]string{"user_id": retUser.UserID.String(), "username": retUser.Username})
}
