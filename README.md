# User API with GO, Chi and Postgres




## Running with docker on port 8080

Run docker

```
docker compose up -d --build 
```
To stop docker
```
docker compose down
```


## Building test pipeline
#### Note if you copied TestMain from https://github.com/ory/dockertest
change this part in TestMain from
```
hostAndPort := resource.GetHostPort("5432/tcp")
databaseUrl := fmt.Sprintf("postgres://user_name:secret@%s/dbname?sslmode=disable", hostAndPort)
```
to

```
port := resource.GetPort("5432/tcp")
databaseUrl := fmt.Sprintf("postgres://user_name:secret@docker:%s/dbname?sslmode=disable", port)
```

as its now running on docker and not locally


## Environment

```POSTGRES_CONN```: Postgres connection string
