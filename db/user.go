package db

import (
	"context"
	"errors"
	"log"

	"github/epith/302-backend-user/bob"

	"github.com/aarondl/opt/omit"
	"github.com/google/uuid"
	"github.com/stephenafamo/bob/dialect/psql"
	"github.com/stephenafamo/bob/dialect/psql/sm"
	"golang.org/x/crypto/bcrypt"
)

func (db Database) GetAllUsers() (bob.UserSlice, error) {
	ctx := context.Background()
	users, err := bob.Users.Query(ctx, db.Conn, sm.Columns(bob.Users.Columns().Except("passwordhash"))).All()
	if err != nil {
		return users, err
	}

	return users, nil
}

func (db Database) AddUser(user *bob.User) error {
	ctx := context.Background()
	hash, _ := HashPassword(user.Passwordhash)
	insertedUser, err := bob.Users.Insert(ctx, db.Conn, &bob.UserSetter{
		Username:     omit.From(user.Username),
		Passwordhash: omit.From(hash),
	})
	if err != nil {
		return err
	}
	user.UserID = insertedUser.UserID
	user.Passwordhash = ""
	return nil
}

func (db Database) LoginUser(user *bob.User) error {
	ctx := context.Background()
	user_check, err := bob.Users.Query(ctx, db.Conn, sm.Where(psql.Quote("username").EQ(psql.Arg(user.Username)))).One()
	if err != nil {
		return errors.New("user does not exist")
	}
	match := CheckPasswordHash(user.Passwordhash, user_check.Passwordhash)
	if match {
		user.UserID = user_check.UserID
		return nil
	}
	return errors.New("wrong password")
}

func (db Database) GetUserById(userId uuid.UUID) (*bob.User, error) {
	ctx := context.Background()
	user, err := bob.FindUser(ctx, db.Conn, userId, "user_id", "username")
	return user, err
}

func (db Database) DeleteUser(userId uuid.UUID) error {
	ctx := context.Background()
	user, err := bob.FindUser(ctx, db.Conn, userId)
	if err != nil {
		return errors.New("user does not exist")
	}
	err = user.Delete(ctx, db.Conn)
	if err != nil {
		return errors.New("unable to delete record")
	}
	return err
}

func (db Database) UpdateUser(userId uuid.UUID, userData *bob.User) (*bob.User, error) {
	ctx := context.Background()
	tx, err := db.Conn.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := tx.Rollback(); err != nil {
			log.Println(err)
		  }
	}()

	user, err := bob.FindUser(ctx, tx, userId)
	if err != nil {
		return userData, err
	}
	var updateUser = &bob.UserSetter{}
	if len(userData.Passwordhash) > 0 {
		hash, _ := HashPassword(userData.Passwordhash)
		updateUser.Passwordhash = omit.From(hash)
	}

	if len(userData.Username) > 0 {
		updateUser.Username = omit.From(userData.Username)
	}

	err = user.Update(ctx, tx, updateUser)
	if err != nil {
		return userData, err
	}

	userData.UserID = userId
	userData.Passwordhash = ""
	if err := tx.Commit(); err != nil {
		return nil, err
	}

	return userData, nil

}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
