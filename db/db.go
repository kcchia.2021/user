package db

import (
	"context"
	"fmt"
	"log"

	_ "github.com/lib/pq"
	"github.com/stephenafamo/bob"
)

var ErrNoMatch = fmt.Errorf("no matching record")

type Database struct {
	Conn bob.DB
}

func Initialize(dbConn string) (Database, error) {
	db := Database{}
	conn, err := bob.Open("postgres", dbConn)
	if err != nil {
		return db, err
	}
	db.Conn = conn
	err = db.Conn.PingContext(context.Background())
	if err != nil {
		return db, err
	}
	log.Println("Database connection established")
	return db, nil
}
