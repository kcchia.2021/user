package db

import (
	"context"
	"fmt"
	"github/epith/302-backend-user/bob/factory"
	"os"
	"testing"
	"time"

	"github.com/google/uuid"
	_ "github.com/lib/pq"
	"github.com/ory/dockertest/v3"
	"github.com/ory/dockertest/v3/docker"
	log "github.com/sirupsen/logrus"
	"github.com/stephenafamo/bob"
	"gotest.tools/v3/assert"
)

var db_test Database

const tableCreationQuery = `
DROP TABLE IF EXISTS users;
CREATE EXTENSION pgcrypto;
CREATE TABLE IF NOT EXISTS users 
(
	user_id uuid not null default gen_random_uuid (),
    username character varying not null,
    passwordhash character varying not null,
    constraint users_pkey primary key (user_id),
    constraint users_username_key unique (username)
  ) tablespace pg_default;
`

func setupTest(tb testing.TB) func(tb testing.TB) {
	//insert 3 users with username: Epith, Omurice, Yum Yum Banana
	insertUsers()
	// Return a function to teardown the test
	return func(tb testing.TB) {
		clearTable()
	}
}

func ensureTableExists() {
	if _, err := db_test.Conn.ExecContext(context.Background(), tableCreationQuery); err != nil {
		log.Fatal(err)
	}
}

func clearTable() {
	if _, err := db_test.Conn.ExecContext(context.Background(), "DELETE FROM users"); err != nil {
		log.Fatal(err)
	}
}

func insertUsers() {
	f := factory.New()
	uuid1, _ := uuid.Parse("c57d1af4-7620-4749-aeef-1ca76779f28b")
	uuid2, _ := uuid.Parse("804a790e-c908-499b-ab87-16e4a9d87fe0")
	uuid3, _ := uuid.Parse("6a70e3ae-c4f5-4b87-9c7e-d75aa9b9d37c")
	uuid4, _ := uuid.Parse("a593743c-afce-4971-bf28-b9a9ad0e887c")
	userTemplate1 := f.NewUser(factory.UserMods.UserID(uuid1), factory.UserMods.Username("Epith"), factory.UserMods.Passwordhash("$2a$14$hbIcJjvo7YkxmcSQw6QSJuNKc6VlrNSTVKgM7j/QsyzbV276uXbVi"))
	userTemplate2 := f.NewUser(factory.UserMods.UserID(uuid2), factory.UserMods.Username("Yum Yum Banana"))
	userTemplate3 := f.NewUser(factory.UserMods.UserID(uuid3), factory.UserMods.Username("Omurice"))
	userTemplate4 := f.NewUser(factory.UserMods.UserID(uuid4), factory.UserMods.Username("ryanpeh"))
	userTemplate1.Create(context.Background(), db_test.Conn)
	userTemplate2.Create(context.Background(), db_test.Conn)
	userTemplate3.Create(context.Background(), db_test.Conn)
	userTemplate4.Create(context.Background(), db_test.Conn)
}

func TestMain(m *testing.M) {

	// uses a sensible default on windows (tcp/http) and linux/osx (socket)
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not construct pool: %s", err)
	}

	err = pool.Client.Ping()
	if err != nil {
		log.Fatalf("Could not connect to Docker: %s", err)
	}

	// pulls an image, creates a container based on it and runs it
	resource, err := pool.RunWithOptions(&dockertest.RunOptions{
		Repository: "postgres",
		Tag:        "11",
		Env: []string{
			"POSTGRES_PASSWORD=secret",
			"POSTGRES_USER=user_name",
			"POSTGRES_DB=dbname",
			"listen_addresses = '*'",
		},
	}, func(config *docker.HostConfig) {
		// set AutoRemove to true so that stopped container goes away by itself
		config.AutoRemove = true
		config.RestartPolicy = docker.RestartPolicy{Name: "no"}
	})
	if err != nil {
		log.Fatalf("Could not start resource: %s", err)
	}

	port := resource.GetPort("5432/tcp")
	databaseUrl := fmt.Sprintf("postgres://user_name:secret@docker:%s/dbname?sslmode=disable", port)

	log.Println("Connecting to database on url: ", databaseUrl)

	resource.Expire(120) // Tell docker to hard kill the container in 120 seconds

	// exponential backoff-retry, because the application in the container might not be ready to accept connections yet
	pool.MaxWait = 120 * time.Second
	if err = pool.Retry(func() error {
		db_test.Conn, err = bob.Open("postgres", databaseUrl)
		if err != nil {
			return err
		}
		return db_test.Conn.PingContext(context.Background())
	}); err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	ensureTableExists()
	//Run tests
	code := m.Run()
	// You can't defer this because os.Exit doesn't care for defer
	if err := pool.Purge(resource); err != nil {
		log.Fatalf("Could not purge resource: %s", err)
	}

	os.Exit(code)
}

func TestGet_UsersEmpty(t *testing.T) {
	users, err := db_test.GetAllUsers()
	if err != nil {
		log.Println(err)
	}
	assert.Equal(t, len(users), 0)
}

func TestGet_AllUsers(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	users, err := db_test.GetAllUsers()
	if err != nil {
		log.Println(err)
	}
	assert.Equal(t, len(users), 4)
	assert.Equal(t, *&users[0].Username, "Epith")
	assert.Equal(t, *&users[1].Username, "Yum Yum Banana")
	assert.Equal(t, *&users[2].Username, "Omurice")
	assert.Equal(t, *&users[3].Username, "ryanpeh")
}

func TestGet_SingleUser(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	uuid, _ := uuid.Parse("c57d1af4-7620-4749-aeef-1ca76779f28b")
	user, err := db_test.GetUserById(uuid)
	if err != nil {
		log.Println(err)
	}
	assert.Equal(t, user.UserID, uuid)
	assert.Equal(t, user.Username, "Epith")
}

func TestGet_SingleInvalidUser(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	uuid, _ := uuid.Parse("93a1fb47-0cba-4c74-8c17-30a42f75f3e9")
	_, err := db_test.GetUserById(uuid)
	if err != nil {
		log.Println(err)
	}
	assert.ErrorContains(t, err, "no rows in result set")
}

func TestPost_SameUsername(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	f := factory.New()
	userTemplate1 := f.NewUser(factory.UserMods.Username("Epith"), factory.UserMods.Passwordhash("hello"))
	err := db_test.AddUser(userTemplate1.Build())
	assert.ErrorContains(t, err, "duplicate key value")
}

func TestPost_DiffUsername(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	f := factory.New()
	userTemplate1 := f.NewUser(factory.UserMods.Username("Epith1"), factory.UserMods.Passwordhash("hello"))
	err := db_test.AddUser(userTemplate1.Build())
	assert.NilError(t, err)
}

func TestPost_Signin(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	f := factory.New()
	userTemplate1 := f.NewUser(factory.UserMods.Username("Epith"), factory.UserMods.Passwordhash("hello"))
	err := db_test.LoginUser(userTemplate1.Build())
	assert.NilError(t, err)
}

func TestPost_SigninInvalidUser(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	f := factory.New()
	userTemplate1 := f.NewUser(factory.UserMods.Username("Epith1"), factory.UserMods.Passwordhash("hello"))
	err := db_test.LoginUser(userTemplate1.Build())
	assert.ErrorContains(t, err, "user does not exist")
}

func TestPost_SigninInvalidPassword(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	f := factory.New()
	userTemplate1 := f.NewUser(factory.UserMods.Username("Epith"), factory.UserMods.Passwordhash("hello1"))
	err := db_test.LoginUser(userTemplate1.Build())
	assert.ErrorContains(t, err, "wrong password")
}

func TestPut_SameUsername(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	f := factory.New()
	uuid, _ := uuid.Parse("c57d1af4-7620-4749-aeef-1ca76779f28b")
	userTemplate1 := f.NewUser(factory.UserMods.Username("Omurice"), factory.UserMods.Passwordhash("hello"))
	_, err := db_test.UpdateUser(uuid, userTemplate1.Build())
	assert.ErrorContains(t, err, "duplicate key value")
}

func TestPut_DiffUsername(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	f := factory.New()
	uuid, _ := uuid.Parse("c57d1af4-7620-4749-aeef-1ca76779f28b")
	userTemplate1 := f.NewUser(factory.UserMods.Username("Epith1"), factory.UserMods.Passwordhash("hello"))
	user, err := db_test.UpdateUser(uuid, userTemplate1.Build())
	assert.NilError(t, err)
	assert.Equal(t, user.UserID, uuid)
	assert.Equal(t, user.Username, "Epith1")
}

func TestPut_InvalidUser(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	f := factory.New()
	uuid, _ := uuid.Parse("93a1fb47-0cba-4c74-8c17-30a42f75f3e9")
	userTemplate1 := f.NewUser(factory.UserMods.Username("Epith1"), factory.UserMods.Passwordhash("hello"))
	_, err := db_test.UpdateUser(uuid, userTemplate1.Build())
	assert.ErrorContains(t, err, "no rows in result set")
}

func TestDelete_ValidUser(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	uuid, _ := uuid.Parse("c57d1af4-7620-4749-aeef-1ca76779f28b")
	err := db_test.DeleteUser(uuid)
	assert.NilError(t, err)
}

func TestDelete_InvalidUser(t *testing.T) {
	teardownTest := setupTest(t)
	defer teardownTest(t)

	uuid, _ := uuid.Parse("93a1fb47-0cba-4c74-8c17-30a42f75f3e9")
	err := db_test.DeleteUser(uuid)
	assert.ErrorContains(t, err, "user does not exist")
}
